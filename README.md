# Challenge-05-Ricky Supriyanto

## ERD ORDER TICKET MOVIE

Berikut adalah erd dari order ticket movie:

![](dokumentasi/erd.png)

## Dokumentasi Routing

![](dokumentasi/routing.png)

## Dokumentasi service.js dan console log

1.  Add Car

    ![](dokumentasi/addcar.png)

2.  Delete Car

    ![](dokumentasi/deletecar.png)

3.  Filter dan Search

    ![](dokumentasi/filter%2Csearch.png)
    ![](dokumentasi/filter.png)

4.  getall

    ![](dokumentasi/getall-find.png)

5.  page add, page update, dan create data

    ![](dokumentasi/pageadd%2Cupdate%2Ccreate.png)

6.  Select all query

    ![](dokumentasi/select-all.png)

7.  Data table

    ![](dokumentasi/table-collum-car.png)

8.  update, delete Car

    ![](dokumentasi/update%2Cdelete-car.png)

9.  Update car

    ![](dokumentasi/updatecar.png)
